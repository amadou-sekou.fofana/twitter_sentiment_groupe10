{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36094201",
   "metadata": {},
   "outputs": [],
   "source": [
    "from nltk.tokenize import RegexpTokenizer\n",
    "def article_tokenize_other(text):\n",
    "    if type(text)!= str:\n",
    "        raise Exception(\"The function takes a string as input data\")\n",
    "    else:\n",
    "        # 1. On extrait les abbreviations\n",
    "        tokenizer = RegexpTokenizer('[a-zA-Z]\\.[a-zA-Z]') #Construction de l'objet tokenizer\n",
    "        abrevtokens = tokenizer.tokenize(text) #application de la methode tokenize() a l'objet lui permettant d'extraire les abreviations\n",
    "        # 2. On extrait les mots et les nombres\n",
    "        tokenizer = RegexpTokenizer('[a-zA-Z]{2,}|\\d+\\S?\\d*')\n",
    "        wordtokens = tokenizer.tokenize(text)\n",
    "        return abrevtokens + wordtokens\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "551dd8bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# représentation des données : choix TF-IDF\n",
    "# construction de l'index inversé\n",
    "from collections import OrderedDict\n",
    "import pickle\n",
    "import math\n",
    "def inverse_index():    \n",
    "    with open('group10_sentiment140_preprocessed.pickle', 'rb') as f1:\n",
    "        OL = pickle.load(f1)\n",
    "    n = 0\n",
    "    index = OrderedDict()\n",
    "    for i in range(len(OL)):\n",
    "        n = n + 1\n",
    "        terms = article_tokenize_other(OL[i]['text'])\n",
    "        for term in terms:\n",
    "            if term in index.keys():\n",
    "                index[term].append(n)\n",
    "            else:\n",
    "                index[term] = list()\n",
    "                index[term].append(n)\n",
    "    return index\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b95268d3",
   "metadata": {},
   "outputs": [],
   "source": [
    "#renvoie la taille d'un dictionnaire : nb de clés\n",
    "def taille_dico(dic):\n",
    "    i = 0\n",
    "    for k in dic:\n",
    "        i+=1\n",
    "    return i"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d087b6d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# prétraitement champ texte\n",
    "import unicodedata\n",
    "import re\n",
    "def normalisation(s):\n",
    "    s = re.sub(r'http\\S+', '', s) # supprime les liens de la chaine\n",
    "    s = re.sub('[^a-zA-Z0-9\\n\\. ]', '', s)\n",
    "    \n",
    "    try:\n",
    "        text = s.lower()\n",
    "        text = unicode(text,'utf-8')\n",
    "    except (TypeError, NameError): # unicode is a default on python 3 \n",
    "        text = unicodedata.normalize('NFD', text).encode('ascii', 'ignore') # supprime les accents de la chaine\n",
    "          \n",
    "    return str(text, 'utf-8')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
